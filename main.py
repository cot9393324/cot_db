import tkinter
from tkinter import ttk
import gspread
from google.oauth2 import service_account
import os
from docxtpl import DocxTemplate
import datetime
from tkinter import messagebox


def save_data():
    date = date_entry.get()
    project_name = project_name_entry.get()
    project_address = project_address_entry.get()

    if not date.strip() or not project_name.strip() or not project_address.strip():
        print("ERROR: One or more values are missing.")
    else:
        print("date:", date)
        print("Project Name:", project_name)
        print("Proponent Name:", project_address)


   # Load the service account credentials from a JSON key file
        credentials = {
            "type": "service_account",
            "project_id": "cotdb-389106",
            "private_key_id": "9675877a7619b34fbe17ca95f0e170cc0c2e18f1",
            "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQClFVBeiAqfbIzo\naL7LxfKGkCUOto24f0Uh9wu5ySfaKqSuu175CaLAX7Ak1zR/gr8duoxyoyRYHPLx\nnFHp9+unTV8U113jbkjV4Xpy8P1swHchPUHRJtqkWqTnkfD7Fc+Uep9OoKRAnGc8\nKlih6UOlcz4/+80e8ECHzTBVik5YrACHubm8m4VebAr0tjFpVU8DYlFPW2lOhq1g\nsBg8NQ+D1VfX6nVTF9tgLuTS1QzcShYMci1jA09OU0PVgpt5bfYbopnj8xa3ju2T\nYYlDPS9kF3vygVRRm/jp30UbI6+BhJWtgURDdiOo28KlePzO9xr7iIea09Px83GI\nRo9Vnq2VAgMBAAECggEABVbAoCYHLeu9TODiVsXCl65+vFCUGcTn7/bE5s/GHaym\nYc5Gfohcvpr5n99j38HTO3tmgmibSsw44S58ibQOZ309l3d1nl7ZNS2TH7eCgqtg\nzu7ju4dlp84UqjaSlBQ6/+LNVyYj2cEVJ6F491eLbATjnP8gPaFofNvaddHAweZU\n24kzCn+6wDUBXqg2ZQONH6y2d6gPx1kuR3wilMwZn1jGok6dO2dYvmVki475HMkn\nhXvm0hYdHsKbyHmi7rFC/rSDYKynLLD1UlK3IRtGr7s6dNJwxQ8NM9f5/6eGhpX6\nqpiAUkDJZ7CgNKub9iZyay2UjlnxuhisTXKii5AjYQKBgQDRDmJapKOfAwbRyb3w\nh4xs4ThyjD2izC7QWiCXoM6sXmLBQmWQxngFgC3z4S++2nnHslaiQzjg8sAJTJQR\nqKGDfIY3y6AbQpmpU+yl89FaWoyiohbPn1e7S6dikEI83RGvodPCuJOicc+PZMVw\nmYhVr45SsgTTeZ/iNvvz9TGpBQKBgQDKJyQ14fISc1cFolqgCCPRA+1XMuXM3HPP\nLQpUtePUkXCocy/L2NQ80DmYeeMQjrxHgtIhgC8Ev37fF4atC2pZYcCEft2J8IDU\nfUsK0+6La0QrBJOiSc+IVcmCFM/pmbLSpp9S8ysEFiFpBqCCJwKZ3DFQ4QSewGK6\n91DEijHXUQKBgFCNHlCWx4TpgaMr/MfFMtJxJXLdnpIv4ondcFl8WKYJ6DtW/hN+\npZhY/1vNFLyRKgNMMB1tkWj7oAr1OshjFXi4bm7pQtO+Ib8g2zpGbBBIe4/erLNr\nTj6Ys/JCa4NQGxR1YkbRH/GjsLortffg8Kszbb9nYHyrjHN5hDNH8s6RAoGAS0da\nJ6qVL6O98z4rS07cr5gryP4OL0Sk57CZGUSGrADhGAmU8e8z0dWkrRAuq1rL6EFi\nKNGIMNdy0nb7UvNeThylyXFI5HYoM3jY+hZDza1LPE2/AH9GBjoUCRng9teijOk5\niXU+Po5TML4kBXOrd0pVHw48XzW7Tm6MqDPiLZECgYEAuxgkmC69MncU8nSNA2/l\nz1WeTGq4u0mH6WdwCIEUQfeduxpd2zhpExQ0sW2GjGEQdEuIpgruMMfOOtQWMZ9+\nNQln3PonoabE7sPm5BRAisSP1he0cFOQZkq0NIPIAsk07JpyQByqzg025NvsZtZ7\njuj9zgWm3ajGcyCEQASItL8=\n-----END PRIVATE KEY-----\n",
            "client_email": "cot-db@cotdb-389106.iam.gserviceaccount.com",
            "client_id": "117058865195436499847",
            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
            "token_uri": "https://oauth2.googleapis.com/token",
            "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
            "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/cot-db%40cotdb-389106.iam.gserviceaccount.com",
            "universe_domain": "googleapis.com"
            }

        # Authenticate using the credentials
        client = gspread.service_account_from_dict(credentials)
     

        # Open the Google Sheet
        sheet = client.open_by_url('https://docs.google.com/spreadsheets/d/1KdQ-1gx1fQmHv7wTxtk2VFHmz0ObLSLECSEAp_Du-l0/edit#gid=0')

        # Select a worksheet (either by index or by title)
        worksheet = sheet.worksheet("Sheet1")

        # Enter data to the sheet
        data = [date, project_name, project_address]
        worksheet.append_row(data)

        date_entry.delete(0, tkinter.END)
        project_name_entry.delete(0, tkinter.END)
        project_address_entry.delete(0, tkinter.END)

        messagebox.showinfo("NOTICE", "SAVED")


def gen_report():
    doc = DocxTemplate("COT_TEMPLATE.docx")
    project_name=project_name_entry.get()
    project_address=project_address_entry.get()

    doc.render({
        "date":datetime.datetime.now().strftime("%m-%d-%Y"),
        "project_name": project_name,
        "project_address": project_address
    })

    doc_name = "new_COT" + project_name + ".docx"
    doc.save(doc_name)

    messagebox.showinfo("NOTICE", "SUCCESSFULLY GENERATED")

window = tkinter.Tk()
window.title("COT_DB")

frame = tkinter.Frame(window)
frame.pack()

# Saving user info = (parent frame, text on the frame )
project_info_frame = tkinter.LabelFrame(frame, text="COT_DB")
project_info_frame.grid(row=0, column=0, padx=20, pady=20)

# VARIABLE = tkinter.Label(frame name, text)
date = tkinter.Label(project_info_frame, text="DATE")
date.grid(row=0, column=0)
project_name = tkinter.Label(project_info_frame, text="COMPANY NAME")
project_name.grid(row=0, column=1)
project_address = tkinter.Label(project_info_frame, text="PROJECT ADDRESS")
project_address.grid(row=0, column=2)

# ENTRY BOX
date_entry = tkinter.Entry(project_info_frame)
project_name_entry = tkinter.Entry(project_info_frame)
project_address_entry = tkinter.Entry(project_info_frame)

date_entry.grid(row=1, column=0)
project_name_entry.grid(row=1, column=1)
project_address_entry.grid(row=1, column=2)

for widget in project_info_frame.winfo_children():
    widget.grid_configure(padx=10, pady=5)

# Button
button = tkinter.Button(frame, text="Save", command=save_data)
button.grid(row=4, column=0, padx=20, pady=5, columnspan=3, sticky="news")
button_gen = tkinter.Button(frame, text="Generate", command=gen_report)
button_gen.grid(row=3, column=0, padx=20, pady=5, columnspan=3, sticky="news")

window.mainloop()
