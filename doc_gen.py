from docxtpl import DocxTemplate

doc = DocxTemplate("COT_TEMPLATE.docx")

context = {
    "date": "6/07/2023",
    "project_name": "ENVICOMM TRUCKING",
    "project_address": "Gov.Cuenco, Banilad, Cebu City"
}


doc.render(context)
doc.save("COT.docx")